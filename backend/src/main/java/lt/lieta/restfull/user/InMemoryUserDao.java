package lt.lieta.restfull.user;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.springframework.stereotype.Repository;

@Repository
public class InMemoryUserDao implements UserDao {
	private final List<User> users = new CopyOnWriteArrayList<>();
	
	public InMemoryUserDao() {
		this.createUser(new User(
			"abc", "Jonas", "Jonaitis", "j.jonaitis@gmail.com"
		));
		this.createUser(new User(
			"abd", "Antanas", "Antanaitis", "a.antanaitis@gmail.com"
		));

	}
	
	@Override
	public List<User> getUsers() {
		return Collections.unmodifiableList(users);
	}

	@Override
	public void createUser(User user) {
		users.add(user);
	}

	@Override
	public User getUser(String userName) {
		return users.stream()
				.filter(user -> user.getUserName() == userName)
				.findFirst()
				.get();
	}

	@Override
	public void deleteUser(String userName) {
		users.removeIf(user -> user.getUserName().equals(userName));
	}

}
