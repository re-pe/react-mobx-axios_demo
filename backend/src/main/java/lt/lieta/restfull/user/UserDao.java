package lt.lieta.restfull.user;

import java.util.List;

public interface UserDao {
	List<User> getUsers();
	void createUser(User user);
	User getUser(String userName);
	void deleteUser(String userName);
}
