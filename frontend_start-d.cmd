@echo off
echo:
set "ROOT=%cd%"
set "FILENAME=%~n0"
set "LETTER=%FILENAME:frontend_run=%"
if exist frontend\src\App.js del /F frontend\src\App.js
type frontend\src\App%LETTER%.js > frontend\src\App.js
cd frontend
start "NPM" cmd /c npm start
cd %ROOT%
echo Darbas baigtas.
echo:

