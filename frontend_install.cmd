@echo off
echo:
set "ROOT=%cd%"
echo Pradinis katalogas -- %cd%
echo:
set "FILENAME=%~n0"
set "FILENAME=%FILENAME:_install=%"
cd %FILENAME%
echo Darbinis katalogas -- %CD%  
echo:
start /wait "NPM" CMD /c npm install
echo Reikalingi paketai suinstaliuoti.
echo:
cd %ROOT%
pause
